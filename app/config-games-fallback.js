module.exports = [
{
    id : '1',
    key : 'Achtung Die Kurve',
    value : {
        abbreviation : 'ADK',
        controller : '/storage/games/AchtungDieKurve/controller.html',
        name : 'Achtung Die Kurve',
        screen : '/storage/games/AchtungDieKurve/screen.html',
        icon : '/storage/games/AchtungDieKurve/game-icon.png'
    }
},
{
    id : '2',
    key : 'Pong',
    value : {
        abbreviation : 'PON',
        controller : '/storage/games/Pong/controller.html',
        name : 'Pong',
        screen : '/storage/games/Pong/screen.html',
        icon : '/storage/games/Pong/game-icon.png'
    }
}
];
