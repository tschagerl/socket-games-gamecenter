module.exports = {
    views: {
        title: 'Game Center',
        baseUrl: '/'
    },

    server: {
        addr: '',
        port: 3000,
        publicPort: 3000,
        origins: '*'
    }
};
