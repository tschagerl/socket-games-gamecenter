module.exports = function (config, app, games, availableScreens) {

    app.get('/', function (req, res) {
        res.render('gamecenter/screen', {
            config: config,
            games
        });
    });

    app.get('/spectator/:gamecenterScreenId', function (req, res) {
        res.render('gamecenter/spectator', {
            config: config,
            games,
            gamecenterScreenId: req.params.gamecenterScreenId
        });
    });

    app.get('/controller/:gamecenterScreenId', function (req, res) {
        res.render('gamecenter/controller', {
            config: config,
            games,
            gamecenterScreenId: req.params.gamecenterScreenId
        });
    });

    app.get(['/spectator', '/s'], function (req, res) {
        var gameScreens = Object.keys(availableScreens).filter(function (value) {
            return !(typeof value !== 'string' || value.match(/-game$/));
        });

        if (gameScreens.length === 1) {
            res.redirect(302, '/spectator/' + gameScreens[0]);
            return;
        }

        res.render('gamecenter/screenList', {
            config: config,
            availableScreens: gameScreens,
            type: 'spectator'
        });
    });

    app.get(['/controller', '/c'], function (req, res) {
        var gameScreens = Object.keys(availableScreens).filter(function (value) {
            return !(typeof value !== 'string' || value.match(/-game$/));
        });

        if (gameScreens.length === 1) {
            res.redirect(302, '/controller/' + gameScreens[0]);
            return;
        }

        res.render('gamecenter/screenList', {
            config: config,
            availableScreens: gameScreens,
            type: 'controller'
        });
    });

    app.get('/screens', (req, res) => {
        let screens = Object.keys(availableScreens).filter(function (screenId) {
            return !(typeof screenId !== 'string' || screenId.match(/-game$/));
        });
        res.json(screens);
    });

};
