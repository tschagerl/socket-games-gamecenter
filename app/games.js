const fs = require('fs');
const path = require('path');

const walk = function (dir, filename) {
    let results = [];
    let list = fs.readdirSync(dir);

    list.forEach(function (file) {
        file = path.resolve(dir, file);
        const stat = fs.statSync(file);

        if (stat && stat.isDirectory()) {
            results = results.concat(walk(file, filename));
        } else {
            const filenameOnly = path.basename(file);
            if (filenameOnly === filename) {
                results.push(file);
            }
        }
    });

    return results;
};

const loadGames = function (baseDir, filename) {
    const result = [];

    const files = walk(baseDir, filename);
    files.forEach(function (file) {
        let filePath = path.relative(baseDir, file);
        let directory = path.dirname(filePath);

        const gameConfig = require.main.require(file);
        gameConfig.directory = directory;
        result.push(gameConfig);
    });

    return result;
};

module.exports = loadGames;
