const connectedClients = {};

module.exports = function (io, availableScreens) {

    function generateScreenID(length) {
        var text = '';
        var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var len = (typeof length === 'number') ? length : 5;

        for (var i = 0; i < len; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }

        return text;
    }

    /*
     * init socket io logic
     */
    io.on('connection', function (socket) {
        // new socket connection
        console.log('a client connected: ' + socket.id);
        connectedClients[socket.id] = socket;

        // GAME CENTER
        var type = 'none'; // none, screen, controller
        var connectedScreen;
        var _screenId;

        // client creates new screen
        socket.on('create-screen', function (screenId, callback) {
            if (typeof screenId !== 'string' || screenId in availableScreens) {
                do {
                    screenId = generateScreenID(5);
                } while (screenId in availableScreens);
            }
            if (typeof screenId !== 'string' || !screenId.match(/^[ A-Za-z0-9_@.\/#&+-]{4,20}$/)) {
                callback({
                    error: true,
                    errorMsg: 'screenId invalid'
                });
                return;
            }
            availableScreens[screenId] = socket;
            _screenId = screenId;
            type = 'screen';
            callback({
                error: false,
                screenId: screenId
            });
            console.log('client ' + socket.id + ' created new screen: ' + screenId);
        });

        // client creates controller
        socket.on('join-screen', function (screenId, callback) {
            if (screenId in availableScreens) {
                type = 'controller';
                // remember screenSocket
                connectedScreen = availableScreens[screenId];
                // send screen the message, that a new controller connected
                connectedScreen.emit('controller-connected', socket.id, () => {
                    // screen acknowledges connection
                    callback({
                        error: false
                    });
                });
                // join in screen room, to give screen the possibility to broadcast
                socket.join(connectedScreen.id);
                console.log('controller ' + socket.id + ' connected to screen: ' + screenId);
            } else {
                callback({
                    error: true,
                    errorMsg: 'screen not found'
                });
            }
        });

        // controller sends custom event to screen
        socket.on('controller-event', function (data, callback) {
            if (type !== 'controller') {
                return;
            }
            data.controllerId = socket.id;
            if (typeof callback === 'function') {
                connectedScreen.emit('controller-event', data, function (data) {
                    callback(data);
                });
            } else {
                connectedScreen.emit('controller-event', data);
            }
        });

        // screen sends custom event to specific client or all connected controllers
        socket.on('screen-event', function (data, callback) {
            if (type !== 'screen') {
                return;
            }
            let target;
            if (data.clientId) {
                if (!connectedClients[data.clientId]) {
                    return;
                }
                target = connectedClients[data.clientId];
            } else {
                target = socket.broadcast.to(socket.id);
            }
            if (typeof callback === 'function') {
                target.emit('screen-event', data, function (data) {
                    callback(data);
                });
            } else {
                target.emit('screen-event', data);
            }
        });

        // if client disconnects, tell others
        socket.on('disconnect', function () {
            if (type === 'controller') {
                connectedScreen.emit('controller-disconnected', socket.id);
                console.log('controller disconnected: ' + socket.id);
            } else if (type === 'screen') {
                socket.broadcast.to(socket.id).emit('screen-disconnected');
                delete availableScreens[_screenId];
                console.log('screen ' + _screenId + ' disconnected by: ' + socket.id);
            } else {
                console.log('a typeless client disconnected: ' + socket.id);
            }
        });
    });
};
