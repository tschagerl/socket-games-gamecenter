/*
 * setup
 */

const extend = require('extend'),
    optional = require('optional'),
    config = extend({}, require('./app/config.js'), optional('./app/config-local.js')),
    express = require('express'),
    engines = require('consolidate'),
    http = require('http'),
    socketio = require('socket.io'),

    app = express(),
    server = http.Server(app),
    io = socketio(server);


/*
 * init
 */

app.engine('html', engines.twig);
app.use(express.static('public'));
app.set('views', './app/views');
app.set('view engine', 'html');


/*
 * check local adress
 */

if (config.server.addr.length === 0) {
    try {
        require('dns').lookup(require('os').hostname(), function (err, add) {
            config.server.addr = add;
        });
    } catch (e) {
        config.server.addr = 'localhost';
    }
}


/*
 * load games from database
 */

const games = require('./app/games.js')(__dirname + '/public/storage/games', 'gamecenter.config.json');


/*
 * init http routes and socket-events
 */

const availableScreens = {};
require('./app/routes.js')(config, app, games, availableScreens);
require('./app/sockets.js')(io, availableScreens);


/*
 * start http server
 */

server.listen(config.server.port, function () {
    console.log('listening on: ' + config.server.port);
});
