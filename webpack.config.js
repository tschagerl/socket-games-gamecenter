const path = require('path');
const webpack = require('webpack');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

module.exports = function (env) {
    const config = {
        entry: {
            controller: './src/js/controller/index.js',
            screen: './src/js/screen/index.js',
            spectator: './src/js/spectator/index.js',
        },
        output: {
            filename: '[name].bundle.js',
            path: path.resolve(__dirname, 'public/js/'),
            publicPath: '/js/',
        },
        module: {
            rules: [{
                test: /qrcodejs/,
                use: 'exports-loader?QRCode=QRCode',
            }, {
                test: /\.js$/,
                loader: 'babel-loader',
                options: {
                    presets: ['env'],
                },
                exclude: [/underscore/, /qrcodejs/],
            }],
        },
        resolve: {
            extensions: ['.js', '.json'],
            modules: [
                path.resolve(__dirname, 'src/js'),
                'node_modules',
            ],
            alias: {
                qrcodejs: 'qrcodejs/qrcode'
            }
        },
        plugins: [],
        devtool: 'source-map',
        target: 'web',
        watchOptions: {
            ignored: [/node_modules/]
        }
    };
    if (env && env.production) {
        config.plugins.push(
            new UglifyJSPlugin()
        );
    }
    return config;
};
