import {GameCenterScreen} from 'socket-games-api/screen';
import qrcodejs from 'qrcodejs';
import $ from 'jquery';

import AbstractView from '../common/AbstractView';

export default AbstractView.extend({

    api: null,
    screenId: null,
    currentGame: null,

    initialize: function () {
        this.api = new GameCenterScreen(
            data => this.onScreenCreated(data),
            error => this.showCreationError(error)
        );
    },

    render: function () {
        this.$el.show();
    },

    onScreenCreated: function (data) {
        // check if successfully created -> store own screen id (used to show connection-link)
        this.screenId = data.screenId;
        this.renderScreenId();
        this.showConnectionOverlay();
        this.api.on('controller-connected', controllerId => this.onControllerConnected(controllerId));
        this.api.on('controller-disconnected', controllerId => this.onControllerDisconnected(controllerId));
        this.api.on('controller-set-name', (data, controllerId, callback) => this.onControllerLogin(data, controllerId, callback));
        this.api.on('start-game', data => this.onStartGame(data));
        this.api.on('controller-check-current-game', (data, controllerId, callback) => this.onCheckCurrentGame(callback));

        $('.games .start-game').click(e => {
            e.preventDefault();
            this.onStartGame({
                game: $(e.currentTarget).data('game')
            });
        });
    },

    showCreationError: function (error) {
        // TODO : handle screen creation error!
        console.error('MainView::showCreationError', error);
    },

    renderScreenId: function () {
        // TODO : show big image that explains how to connect smartphone
        // ATTENTION: this method has a duplicate in games
        let $controller = this.$el.find('.controller-link');
        let $spectator = this.$el.find('.spectator-link');

        let url = location.protocol + '//' + $controller.data('url') + 'controller/' + this.screenId;
        JSCONST.gameCenterControllerUrl = url;
        $controller.html('<a href="' + url + '" target="_blank">' + url + '</a>');
        new qrcodejs.QRCode(this.$el.find('.qrcode')[0], url);

        url = location.protocol + '//' + $spectator.data('url') + 'spectator/' + this.screenId;
        $spectator.html('<a href="' + url + '" target="_blank">' + url + '</a>');
    },

    showConnectionOverlay: function () {
        // TODO : show connection overlay
    },

    onControllerConnected: function (controllerId) {
        let userNr = this.$el.find('.connected-users li').length + 1;
        this.$el.find('.connected-users ul').append('<li data-id="' + controllerId + '">Guest #' + userNr + '</li>');
        // TODO : hide connection overlay
        // TODO : show player name in sidebar
    },

    onControllerLogin: function (data, controllerId, callback) {
        this.$el.find('li[data-id="' + controllerId + '"]').html(data.name);
        callback();
    },

    onCheckCurrentGame: function (callback) {
        if (typeof callback === 'function') {
            callback({
                currentGame: this.currentGame
            });
        }
    },

    onStartGame: function (data) {
        let game = data.game;
        if (this.currentGame !== null) {
            return;
        }
        this.currentGame = game;
        let gameURL = this.$el.find('.start-game[data-game="' + game + '"]   ').data('game-url');
        gameURL += (gameURL.indexOf('?') > -1) ? '&screenid=' : '?screenid=';
        gameURL += this.screenId;
        // TODO : check if let game contains a valid game
        // TODO : show loading until iframe fires loading-finished event!
        $('div#game').show().html('<iframe src="' + gameURL + '" data-screenid="' + this.screenId + '">');

        // TODO : use socket-games-api
        window.JSCONST.onGameCreated = () => {
            this.api.broadcast('screen-game-created', {
                currentGame: this.currentGame
            });
        };
        window.JSCONST.stopGame = () => {
            this.currentGame = null;
            $('div#game').hide().html('');
        };
    },


    onControllerDisconnected: function (controllerId) {
        this.$el.find('li[data-id="' + controllerId + '"]').hide();

        // TODO : show connection overlay if last controller left
        if (this.$el.find('.connected-users li').length === 0) {
            $('div#game').hide().html('');
        }
    }

});
