import {GameCenterController} from 'socket-games-api/controller';
import $ from 'jquery';

import AbstractView from '../common/AbstractView';
import UserModel from './UserModel';

export default AbstractView.extend({

    screenId: null,
    api: null,
    user: null,

    DOM: null,

    /*
     * Public Funcs
     */

    initialize: function (options) {
        AbstractView.prototype.initialize.call(this, options);
        this.screenId = this.$el.data('screenid');
        this.user = new UserModel();
        this.DOM = {
            $login: this.$el.find('.login'),
            $loginForm: this.$el.find('form.login'),
            $guestForm: this.$el.find('form.guest'),
            $game: this.$el.find('#game'),
            $loading: this.$el.find('#loading')
        };
    },

    render: function () {
        this.api = new GameCenterController(
            data => this.onControllerCreated(data),
            error => this.showCreationError(error),
            this.screenId
        );
    },

    /*
     * Private Funcs - Callbacks/EventHandlers
     */

    onControllerCreated: function (data) {
        if (data.error) {
            this.showCreationError(data.errorMsg);
            return;
        }

        if (this.user.loggedIn) {
            this.onLoggedIn();
        } else {
            this.showLogin();
        }

        this.api.on('screen-disconnected', () => this.onScreenDisconnected());
        this.api.on('screen-game-created', data => this.onScreenGameCreated(data));
    },

    onLoggedIn: function () {
        this.DOM.$login.hide();
        this.api.emit('controller-set-name', {
            name: this.user.get('name')
        }, () => this.showControls());
    },

    onScreenDisconnected: function () {
        // TODO : remove controls and listeners
        // TODO : show info to user
        console.error('screen disconnected');
    },

    /*
     * Private Funcs
     */

    showCreationError: function (msg) {
        // TODO : handle controller connection error!
        console.error(msg + ' Try to reload the screen.');
    },

    showLogin: function () {
        this.DOM.$login.show();
        this.DOM.$loginForm.off('submit').on('submit', e => {
            e.preventDefault();
            this.user.login(this.DOM.$login.find('input[name="username"]').val(), () => {
                this.onLoggedIn();
            });
        });
        this.DOM.$guestForm.off('submit').on('submit', e => {
            e.preventDefault();
            this.DOM.$login.hide();
            this.showControls();
        });
    },

    showControls: function () {
        this.$el.find('.games').show();

        this.api.emit('controller-check-current-game', {}, data => this.onScreenGameCreated(data));

        // TODO : show controls
        // TODO : create event listeners for game starting, gamecenter navigating etc
        this.$el.find('li.start-game').on('click', e => {
            e.preventDefault();
            let game = $(e.currentTarget).data('game');
            this.api.emit('start-game', {
                game: game
            });
            this.showLoading();
        });
    },

    onScreenGameCreated: function (data) {
        if (data.currentGame === null) {
            return;
        }
        let $gameLi = this.$el.find('.start-game[data-game="' + data.currentGame + '"]');
        if (data.game !== null && $gameLi.length > 0) {
            this.joinGame(data.game, $gameLi.data('game-url'));
        }
    },

    joinGame: function (game, gameURL) {
        gameURL += (gameURL.indexOf('?') > -1) ? '&screenid=' : '?screenid=';
        gameURL += this.screenId;
        this.hideLoading();
        this.DOM.$game.show().html('<iframe src="' + gameURL + '" data-screenid="' + this.screenId + '">');
        window.JSCONST.stopGame = function () {
            $('div#game').hide().html('');
        }.bind(this);
    },

    showLoading: function () {
        this.DOM.$loading.show();
    },

    hideLoading: function () {
        this.DOM.$loading.hide();
    }

});
