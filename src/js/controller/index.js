import Backbone from 'backbone';

import Router from '../common/Router';
import MainView from './MainView';

let mainView = new MainView({
    el: 'body'
});

new Router({
    mainView: mainView
});

Backbone.history.start({
    pushState: false
});
