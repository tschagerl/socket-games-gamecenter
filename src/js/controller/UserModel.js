import Backbone from 'backbone';

export default Backbone.Model.extend({

    loggedIn: false,

    /*
     * Public Funcs
     */

    initialize: function (options) {
        if (localStorage.username) {
            this.set({
                name: localStorage.username
            });
            this.loggedIn = true;
        }
    },

    login: function (name, callback) {
        this.set({
            name: name
        });
        localStorage.username = name;
        callback();
    }

    /*
     * Private Funcs - Callbacks/EventHandlers
     */

});
