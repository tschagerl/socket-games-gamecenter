import Backbone from 'backbone';

export default Backbone.View.extend({

    /**
     *
     *  close
     *
     *  cleans view (not used atm)
     *
     */
    close: function () {
        this.undelegateEvents();
        this.$el.removeData().unbind();
        this.remove();

        Backbone.View.prototype.remove.call(this);
    }

});
