import Backbone from 'backbone';

export default Backbone.Router.extend({

    routes: {
        '': 'index'
    },

    mainView: null,

    initialize: function (options) {
        this.mainView = options.mainView;
    },

    index: function () {
        this.mainView.render();
    }

});
