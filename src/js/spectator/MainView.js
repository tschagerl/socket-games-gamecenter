import {GameCenterController} from 'socket-games-api/controller';
import qrcodejs from 'qrcodejs';
import $ from 'jquery';

import AbstractView from '../common/AbstractView';

export default AbstractView.extend({

    screenId: null,
    api: null,

    DOM: null,

    /*
     * Public Funcs
     */

    initialize: function (options) {
        AbstractView.prototype.initialize.call(this, options);
        this.screenId = this.$el.data('screenid');
        this.DOM = {
            $game: this.$el.find('#game'),
            $loading: this.$el.find('#loading')
        };
    },

    render: function () {
        this.api = new GameCenterController(
            data => this.onSpectatorCreated(data),
            error => this.showCreationError(error),
            this.screenId
        );
        this._renderScreenId();
    },

    /*
     * Private Funcs - Callbacks/EventHandlers
     */

    onSpectatorCreated: function (data) {
        if (data.error) {
            this.showCreationError(data.errorMsg);
            return;
        }

        this.showSpectator();

        this.api.on('screen-disconnected', () => this.onScreenDisconnected());
        this.api.on('screen-game-created', data => this.onScreenGameCreated(data));
    },

    onScreenDisconnected: function () {
        // TODO : remove controls and listeners
        // TODO : show info to user
        console.error('screen disconnected');
    },

    /*
     * Private Funcs
     */

    showCreationError: function (msg) {
        // TODO : handle controller connection error!
        console.error(msg + ' Try to reload the screen.');
    },

    showSpectator: function () {
        this.$el.find('.games').show();

        this.api.emit('controller-check-current-game', {}, data => this.onScreenGameCreated(data));

        // TODO : show controls
        // TODO : create event listeners for game starting, gamecenter navigating etc
        this.$el.find('li.start-game').on('click', e => {
            e.preventDefault();
            let game = $(e.currentTarget).data('game');
            this.api.emit('start-game', {
                game: game
            });
            this.showLoading();
        });
    },

    onScreenGameCreated: function (data) {
        if (data.currentGame === null) {
            return;
        }
        let $gameLi = this.$el.find('.start-game[data-game="' + data.currentGame + '"]');
        if (data.game !== null && $gameLi.length > 0) {
            this.joinGame(data.game, $gameLi.data('game-url'));
        }
    },

    joinGame: function (game, gameURL) {
        gameURL += (gameURL.indexOf('?') > -1) ? '&screenid=' : '?screenid=';
        gameURL += this.screenId;
        this.hideLoading();
        this.DOM.$game.show().html('<iframe src="' + gameURL + '" data-screenid="' + this.screenId + '">');
        window.JSCONST.stopGame = function () {
            $('div#game').hide().html('');
        }.bind(this);
    },

    showLoading: function () {
        this.DOM.$loading.show();
    },

    hideLoading: function () {
        this.DOM.$loading.hide();
    },

    _renderScreenId: function () {
        // TODO : show big image that explains how to connect smartphone
        let $controller = this.$el.find('.controller-link');

        let url = location.origin + '/controller/' + this.screenId;
        JSCONST.gameCenterControllerUrl = url;
        $controller.html('<a href="' + url + '" target="_blank">' + url + '</a>');
        new qrcodejs.QRCode(this.$el.find('.qrcode')[0], url);
    }

});
